// Basic: 

/*

function createNewUser(firstName, lastName){
    firstName = prompt('Firstname: ');
    lastName = prompt('Lastname: ');
    let newUser = {
        firstname: firstName,
        lastname: lastName,
        login: '',
        getLogin() {
            let login = firstName[0].toLowerCase() + lastName.toLowerCase();
            return login
        },

    }
	return newUser;
}

let user = createNewUser();
console.log(user.getLogin()); 

*/



//Advance:


function createNewUser(){

    let newUser = {
        firstname: '',
        lastname: '',
        setFirstName(newName) {
            Object.defineProperty(newUser, 'firstname', {value: this.firstname = newName})
        },
        setLastName(newLName) {
            Object.defineProperty(newUser, 'lastname', {value: this.lastname = newLName}) 
        },
        getLogin() {
            let login = this.firstname[0].toLowerCase() + this.lastname.toLowerCase();
            return login
        },
    }
    Object.defineProperty(newUser, 'firstname', { value: prompt('Firstname: '), writable: false, configurable: true });
    Object.defineProperty(newUser, 'lastname', { value: prompt('Lastname: '), writable: false, configurable: true }); 
	return newUser;

}


// Test
let user = createNewUser();
console.log(user.getLogin());

console.log(user);

user.firstname = 'Test1';
user.lastname = 'Test2';

console.log(user);

user.setFirstName('newFirstName');
user.setLastName('newLastName');

console.log(user);

